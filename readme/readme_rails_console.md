# GitLab rails console

At the heart of GitLab is a web application built using the Ruby on Rails framework. The Rails console. provides a way to interact with your GitLab instance from the command line, and also grants access to the amazing tools built right into Rails.

```bash
docker exec -it <container-id> gitlab-rails console
```

references:<https://docs.gitlab.com/ee/administration/operations/rails_console.html?tab=Docker>
