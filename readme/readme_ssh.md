# SSH Keys configuration - For easy to do the following command

1. Generate key pairs

```bash
ssh-keygen -t rsa -C example@mail.com
```

2. Drop `id_rsa.pub` from local to `~/.ssh/authorized_keys` of server

3. Testing connection

```bash
ssh $USER@$IP_ADDRESS
```

## GitLab-sshd

in case of linux:
When running `docker compose up -d` error will be returned that you have already bind 22 port to another service which is sshd

in case of windows:
if error is not appear = no need to config this

**ChatGPT offer the options 🚧:**

**Option 1**: Use a different port for SSH in the Docker container:
As previously mentioned, you can map a different port on the host to port 22 inside the Docker container. For example, you can use port 2222 on the host:

and when you ssh to git@x.x.x.x you should:

On docker run command:

```bash
   --publish 443:443 --publish 80:80 --publish 2222:22 \
```

```bash
   ssh -p 2222 -i ~/.ssh/id_rsa git@x.x.x.x
```

OR

_Terminate 22 port / SSH service on your server 🥲_

**Option 2**: Terminate the existing SSH server and use port 22 inside the Docker container:
If you have the flexibility to stop the SSH server on the host machine and are willing to temporarily interrupt SSH access to the host, you can proceed as follows:

Verfiy port 22:

```bash
   sudo lsof -i :22
```

Stop the existing SSH server on the host:

```bash
   sudo service ssh stop
```

**Option 3** [💚CURRENTLY]: Change ssh of server to another port (2222) and use port 22 for gitlab container:

To change the default SSH port from 22 to 2222, you need to modify the SSH server configuration file. On most Linux distributions, including Rocky Linux, the SSH server configuration file is located at /etc/ssh/sshd_config. Follow these steps to change the default SSH port:

Open the SSH server configuration file with a text editor:

```bash
sudo vi /etc/ssh/sshd_config
```

If you prefer using a different text editor, replace vi with the name of your preferred text editor (e.g., nano, vim, etc.).

Find the line that specifies the default SSH port (Port 22) and change it to the desired port (Port 2222):

```yaml
#Port 22
Port 2222
```

Remove the # symbol if it exists before the Port 22 line to uncomment it.

Save the changes and exit the text editor.

Restart the SSH service to apply the changes:

```bash
sudo systemctl restart sshd
```

After making these changes, the SSH server will listen on port 2222 instead of the default port 22. Keep in mind that if you change the SSH port, you will need to use the new port number whenever you connect to the server using SSH. For example, to SSH into the server, you would use:

```bash
ssh -p 2222 user@server_ip_or_hostname
```

Replace user with your username and server_ip_or_hostname with the IP address or hostname of your server.

**Option 4** [🧡GitLab recommended] Change gitlab_sshd port to 2222 by gitlab_omnibus

gitlab-sshd is a lightweight alternative to OpenSSH for providing SSH operations. While OpenSSH uses a restricted shell approach, gitlab-sshd behaves more like a modern multi-threaded server application, responding to incoming requests. The major difference is that OpenSSH uses SSH as a transport protocol while gitlab-sshd uses Remote Procedure Calls (RPCs). See the blog post for more details.

The following instructions enable `gitlab-sshd` on a different port than `OpenSSH`:

Edit `/etc/gitlab/gitlab.rb`:

```bash
gitlab_sshd['enable'] = true
gitlab_sshd['listen_address'] = '[::]:2222' # Adjust the port accordingly
```

Optional. By default, Linux package installations generate SSH host keys for `gitlab-sshd` if they do not exist in `/var/opt/gitlab/gitlab-sshd`. If you wish to disable this automatic generation, add this line:

```bash
gitlab_sshd['generate_host_keys'] = false
```

Save the file and reconfigure GitLab:

```bash
sudo gitlab-ctl reconfigure
```

By default, gitlab-sshd runs as the git user. As a result, gitlab-sshd cannot run on privileged port numbers lower than 1024. This means users must access Git with the gitlab-sshd port, or use a load balancer that directs SSH traffic to the gitlab-sshd port to hide this.

So, file `docker-compose.yml` will changed due to the gitlab_sshd port

```yaml
version: "3.8"

services:
  gitlab:
    image: gitlab/gitlab-ce:latest
    container_name: gitlab
    environment:
      GITLAB_OMNIBUS_CONFIG: "external_url 'http://192.168.1.57/'; gitlab_rails['lfs_enabled'] = true; gitlab_sshd['enable'] = true; gitlab_sshd['listen_address'] = '[::]:2222'; gitlab_sshd['generate_host_keys'] = false;" # gitlab_sshd will added
    restart: always
    ports:
      - "443:443"
      - "80:80"
      - "2222:22" # Change this line to map port 2222 of the host to port 22 of the container
    volumes:
      - $GITLAB_HOME/config:/etc/gitlab
      - $GITLAB_HOME/logs:/var/log/gitlab
      - $GITLAB_HOME/data:/var/opt/gitlab
    dns: 8.8.8.8
    shm_size: 4096m
    networks:
      - onpremises-net
  registry:
    image: registry:latest
    container_name: private_registry
    restart: always
    ports:
      - "$IP_ADDRESS:5000:5000"
    volumes:
      - $DOCKER_REGISTRY/data:/var/lib/registry
    environment:
      REGISTRY_HTTP_ADDR: 0.0.0.0:5000
    networks:
      - onpremises-net

networks:
  onpremises-net:
    driver: bridge
```

Users may see host key warnings because the newly-generated host keys differ from the OpenSSH host keys. Consider disabling host key generation and copy the existing OpenSSH host keys into `/var/opt/gitlab/gitlab-sshd` if this is an issue.

reference:<https://docs.gitlab.com/ee/administration/operations/gitlab_sshd.html>

3.Run script file

```bash
   bash gitlab-build
```

GitLab Docs from gitlab-sshd

gitlab-sshd is a standalone SSH server written in Go. It is provided as a part of the gitlab-shell package. It has a lower memory use as a OpenSSH alternative, and supports group access restriction by IP address for applications running behind the proxy.

The capabilities of GitLab Shell are not limited to Git operations.

Reference:<https://docs.gitlab.com/ee/administration/operations/gitlab_sshd.html>
